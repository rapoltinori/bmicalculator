package com.healthandfitness.bmicalculator.measurement;

import com.healthandfitness.bmicalculator.exception.NegativeWeightException;
import com.healthandfitness.bmicalculator.exception.WeightUnitException;
import org.junit.Before;
import org.junit.Test;

public class WeightExceptionTest {
    private Weight weight;

    @Before
    public void setUp() {
        weight = new Weight();
    }

    @Test(expected = NegativeWeightException.class)
    public void testToKilogramIfWeightIsNotNegative() throws WeightUnitException, NegativeWeightException {
        weight.setValue(-65.0);
        weight.setUnitW(WeightType.kg);
        double result = weight.toKilogram();
    }
}
