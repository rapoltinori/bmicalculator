package com.healthandfitness.bmicalculator.measurement;

import com.healthandfitness.bmicalculator.exception.HeightUnitException;
import com.healthandfitness.bmicalculator.exception.NegativeHeightException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class HeightTest {
    private Height height;

    @Before
    public void setUp() {
        height = new Height();
    }

    @Test
    public void testToMeterIfHeightIsNotNegative() throws HeightUnitException, NegativeHeightException {
        height.setValue(170.0);
        height.setUnitH(HeightType.cm);
        double result = height.toMeter();

        assertEquals(1.7, result, 0.01);
    }
}
