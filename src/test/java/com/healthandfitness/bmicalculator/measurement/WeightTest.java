package com.healthandfitness.bmicalculator.measurement;

import com.healthandfitness.bmicalculator.exception.NegativeWeightException;
import com.healthandfitness.bmicalculator.exception.WeightUnitException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WeightTest {

    private Weight weight;

    @Before
    public void setUp() {
        weight = new Weight();
    }

    @Test
    public void testToKilogramIfWeightIsNotNegative() throws WeightUnitException, NegativeWeightException {
        weight.setValue(65.0);
        weight.setUnitW(WeightType.kg);
        double result = weight.toKilogram();

        assertEquals(65.0, result, 0.01);
    }
}
