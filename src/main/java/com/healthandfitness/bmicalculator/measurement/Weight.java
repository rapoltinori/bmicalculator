package com.healthandfitness.bmicalculator.measurement;

import com.healthandfitness.bmicalculator.exception.NegativeWeightException;
import com.healthandfitness.bmicalculator.exception.WeightUnitException;

public class Weight {
	private double value;
	private WeightType unitW;

	public Weight(){

	}
	
	public Weight(double value, WeightType unit) {
		this.value = value;
		this.unitW = unit;
	}
	
	public double toKilogram() throws WeightUnitException, NegativeWeightException {
		if (value <= 0) {
			throw new NegativeWeightException("Enter a positive number");
		}

		if (unitW == WeightType.kg) {
			return value;
		} else if (unitW == WeightType.g) {
			return value / 1000;
		}
		else if (unitW == WeightType.pounds) {
			return value * 2.20462262;
		}else {
			throw new WeightUnitException("Wrong weight unit");
		}
	}
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public WeightType getUnitW() {
		return unitW;
	}

	public void setUnitW(WeightType unitW) {
		this.unitW = unitW;
	}
}
