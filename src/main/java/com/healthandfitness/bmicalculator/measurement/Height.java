package com.healthandfitness.bmicalculator.measurement;

import com.healthandfitness.bmicalculator.exception.HeightUnitException;
import com.healthandfitness.bmicalculator.exception.NegativeHeightException;

public class Height {
	private double value;
	private HeightType unitH;

	public Height(){

	}
	
	public Height(double value, HeightType unit) {
		this.value = value;
		this.unitH = unit;
	}
	
	public double toMeter() throws HeightUnitException, NegativeHeightException {
		if (value <= 0) {
			throw new NegativeHeightException("Enter a positive number");
		}

		if (unitH == HeightType.m) {
			return value;
		} else if (unitH == HeightType.cm) {
			return value / 100;
		} else if (unitH == HeightType.inches) {
			return value * 0.0254;
		} else {
			throw new HeightUnitException("Wrong height unit");
		}
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}

	public HeightType getUnitH() {
		return unitH;
	}

	public void setUnitH(HeightType unitH) {
		this.unitH = unitH;
	}
}
