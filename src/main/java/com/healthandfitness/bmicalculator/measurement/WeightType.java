package com.healthandfitness.bmicalculator.measurement;

public enum WeightType {
	kg,
	g,
	pounds
}
