package com.healthandfitness.bmicalculator.measurement;

public enum HeightType {
	cm,
	m,
	inches
}