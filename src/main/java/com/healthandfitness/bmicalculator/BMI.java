package com.healthandfitness.bmicalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BMI {
	private double value;
	private String category;
	
	public BMI(double value, String category) {
		this.value = value;
		this.category = category;
	}
	
	public double getValue() {
		double precisionValue = BigDecimal.valueOf(value)
				.setScale(2, RoundingMode.HALF_UP)
				.doubleValue();
		return precisionValue;
	}
	
	public String getCategory() {
		return category;
	}	
}
