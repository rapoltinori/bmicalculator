package com.healthandfitness.bmicalculator;

import com.healthandfitness.bmicalculator.exception.MeasurementException;
import com.healthandfitness.bmicalculator.helpers.BMICreator;
import com.healthandfitness.bmicalculator.measurement.Height;
import com.healthandfitness.bmicalculator.measurement.HeightType;
import com.healthandfitness.bmicalculator.measurement.Weight;
import com.healthandfitness.bmicalculator.measurement.WeightType;

public class BMICalculator {
	private BMI bmi;
	private BMICreator BMICreator;

	public BMICalculator() {
		BMICreator = new BMICreator();
	}

	public BMI getBMI(double height, HeightType unitH, double weight, WeightType unitW) throws MeasurementException {
		Height heightWithUnit = new Height(height, unitH);
		Weight weightWithUnit = new Weight(weight, unitW);
		bmi = BMICreator.getBMI(heightWithUnit, weightWithUnit);
		return bmi;
	}
}
