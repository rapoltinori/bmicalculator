package com.healthandfitness.bmicalculator.helpers;

import java.util.ArrayList;

public class Categorize {
	BMICreator bmiCreator = new BMICreator();
	
	public String categorizeBMI(double bmi) {
		String categorize = null;
		ArrayList<String> category = new ArrayList<String>();
		
		category.add("Severe Thinness");
		category.add("Moderate Thinness");
		category.add("Mild Thinness");
		category.add("Normal");
		category.add("Overweight");
		category.add("Obese Class I");
		category.add("Obese Class II");
		category.add("Obese Class III");
		
		if (bmi < 16) {
			categorize = category.get(0);
		} 
		if (bmi >= 16 && bmi < 17) {
			categorize = category.get(1);
		} 
		if (bmi >= 17 && bmi < 18.5) {
			categorize = category.get(2);
		} 
		if (bmi >= 18.5 && bmi < 25) {
			categorize = category.get(3);
		} 
		if (bmi >= 25 && bmi < 30) {
			categorize = category.get(4);
		} 
		if (bmi >= 30 && bmi < 35) {
			categorize = category.get(5);
		} 
		if (bmi >= 35 && bmi < 40) {
			categorize = category.get(6);
		} 
		if (bmi >= 40) {
			categorize = category.get(7);
		}
		
		return categorize;
		
	}
}
