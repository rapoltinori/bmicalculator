package com.healthandfitness.bmicalculator.helpers;

import com.healthandfitness.bmicalculator.BMI;
import com.healthandfitness.bmicalculator.measurement.Height;
import com.healthandfitness.bmicalculator.measurement.Weight;
import com.healthandfitness.bmicalculator.exception.MeasurementException;

public class BMICreator {
	
	private BMI bmi;
	private double bmiValue;

	private double calculateBMI(double height, double weight) {
		return weight / (Math.pow(height, 2));
	}
	
	public BMI getBMI(Height height, Weight weight) throws MeasurementException {
		Categorize categorize = new Categorize();
		bmiValue = calculateBMI(height.toMeter(), weight.toKilogram());
		String category = categorize.categorizeBMI(bmiValue);

		bmi = new BMI(bmiValue, category);
		return bmi;
	}
}
