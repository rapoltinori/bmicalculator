package com.healthandfitness.bmicalculator.exception;

public class HeightUnitException extends MeasurementException {

    public HeightUnitException(String message) {
        super(message);
    }
}
