package com.healthandfitness.bmicalculator.exception;

public class MeasurementException extends Exception {

    public MeasurementException(String message) {
        super(message);
    }
}