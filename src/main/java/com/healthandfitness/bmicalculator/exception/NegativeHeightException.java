package com.healthandfitness.bmicalculator.exception;

public class NegativeHeightException extends MeasurementException {

    public NegativeHeightException(String message) {
        super(message);
    }
}