package com.healthandfitness.bmicalculator.exception;

public class NegativeWeightException extends MeasurementException {

    public NegativeWeightException(String message) {
        super(message);
    }
}