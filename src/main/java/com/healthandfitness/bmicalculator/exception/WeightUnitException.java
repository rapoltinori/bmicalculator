package com.healthandfitness.bmicalculator.exception;

public class WeightUnitException extends MeasurementException {

    public WeightUnitException(String message) {
        super(message);
    }
}
